const bcrypt = require('bcrypt');
const salt = 10;


const bcrypt_function = {
    plaintext_hash:(plaintext)=>{
        return new Promise(function(resolve,reject){
            bcrypt.hash(plaintext,salt,function(err,hash){
                if(err){
                    reject(err);
                }else{
                    resolve(hash);
                }
            })
        })
    },
    hash_plaintext:(hashtext,plaintext)=>{
    return new Promise(function(resolve,reject){
    bcrypt.compare(plaintext,hashtext,function(err,result){
        if(err){
            reject(err);
        }else{
            resolve(result);
        }
    })
})
    }
}
module.exports = bcrypt_function;