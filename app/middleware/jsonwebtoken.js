const jsonwebtoken = require('jsonwebtoken');
const secret = 'supersecret';


const jwt = {
create_jwt:function(data){
       let token =  jsonwebtoken.sign({id:data.uuid,email:data.email},secret,{
            expiresIn:86400
        })
        return token;
},
verify_jwt:function(req,res,next){
    if(!req.headers.authorization){
        res.status(401).json({status:401,message:'no token provided'});
    }else{
        jsonwebtoken.verify(req.headers.authorization,secret,function(err,decoded){
        if(err){
            res.status(500).json({status:500,'message':'authentication failed'});
        }else{
            next();
        }
         })
    }
        
    
}
}

module.exports = jwt;