const connection = require('../connection');
connection.connect();

const mysql = {

    // query function 
    query_function: (query, data) => {
        return new Promise(function(resolve, reject) {
            connection.query(query, data, function(err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        })
    }
}
module.exports = mysql;