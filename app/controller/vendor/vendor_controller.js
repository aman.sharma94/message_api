const mysql = require('../../middleware/mysql');
const bcrypt = require('../../middleware/bcrypt');
const uuid =  require('uuid');
const mailer = require('../../middleware/node_mailer');
const jwt = require('../../middleware/jsonwebtoken');

const user_controller = {

    sign_up: async(req, res, next) => {
        try {
            let query = "INSERT INTO vendors (`fullName`,`email`,`password`,`mobileNo`,`uuid`) values (?,?,?,?,?)";
            let data = [];
            let hash = await bcrypt.plaintext_hash(req.body.password);
            data[0] = req.body.fullName;
            data[1] = req.body.email;
            data[2] = hash;
            data[3] = req.body.mobileNo;
            data[4]= uuid.v1();
            
            const result = await mysql.query_function(query, data);
            const mail = await  mailer.send_mail(req.body.email,'Register Mail','Registed successfully')
            res.json({ status: 200, message: 'User Registerted' })
        } catch (error) {
            if(error.code=="ER_DUP_ENTRY"){
                if(error.sqlMessage.includes('mobileNo')){
                    res.status(200).json({ status: 409, message: 'Mobile Number is already registered with us.' })
                }else{
                    res.status(200).json({ status: 409, message: 'Email Address is already registered with us.' })
                }
            }else{
                res.status(200).json({ status: 409, message: error })
            }
           
        }

    },
    signin:async (req,res,next)=>{
        try{
            let query = "SELECT `uuid` as vendor_id,`fullName`,`email`,`password`,`mobileNo` FROM vendors where `email`=?";
            let data = [req.body.email];
            const checkUser = await mysql.query_function(query,data);
            if(checkUser.length==0){
                res.status(200).json({status:404,message:"Email not registered with us."})
            }else{
            let checkpassword = await bcrypt.hash_plaintext(checkUser[0].password,req.body.password);
            if(!checkpassword){
                res.status(200).json({status:401,message:'Password not matched'})
            }else{
                delete(checkUser[0].password);;
                let token = jwt.create_jwt(checkUser[0]);
                checkUser[0]['token']= token;
                res.status(200).json({status:200,data:checkUser})
            }
                
            }
            
        }catch(error){
            res.status(200).json({ status: 500, message: error.sqlMessage })
        }
    }
}
module.exports = user_controller;