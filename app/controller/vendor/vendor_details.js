const mysql = require('../../middleware/mysql');
const uuid = require('uuid');

const vendor_details = {
    add_shop: async function(req, res, next) {
        try {
            let query = "INSERT INTO `vendors_shops`( `vendor_id`, `uuid`, `shop_name`, `shop_address`, `shop_phone`, `state`, `location`, `pincode`) VALUES (?,?,?,?,?,?,?,?)";
            let data = [];
            data[0] = req.body.vendor_id;
            data[1] = uuid.v1();
            data[2] = req.body.shop_name;
            data[3] = req.body.shop_address;
            data[4] = req.body.shop_phone;
            data[5] = req.body.state;
            data[6] = req.body.location;
            data[7] = req.body.pincode;
            const result = await mysql.query_function(query, data);
            if (result) {
                let vendor_query = "SELECT  `uuid` as shop_id, `shop_name`, `shop_address`, `shop_phone`, `state`, `location`, `pincode` FROM `vendors_shops` WHERE id= ?";
                let vendor_id = result.insertId
                let vendor_data = await mysql.query_function(vendor_query, vendor_id);
                if (vendor_data.length > 0) {
                    res.status(200).json({ status: 200, message: 'Vendor shop saved', data: vendor_data[0] });
                } else {
                    res.status(200).json({ status: 404, message: 'No vendor Found' });
                }

            }

        } catch (error) {
            res.status(200).json({ status: 409, message: error.sqlMessage })
        }
    },

    add_image: async function(req,res,next){
        if(!req.file){
            res.status(200).json({ status: 404, message: 'No File is selected' });    
        }else{
            let query = "INSERT INTO `vendors_shops_image`( `vendor_id`, `uuid`, `shop_id`, `image_url`) VALUES (?,?,?,?)";
        let data = [];
        data[0]= req.body.vendor_id;
        data[1]= uuid.v1();
        data[2]= req.body.shop_id;
        data[3] =req.file.path.replace(/\\/g,'/')
        const result = await mysql.query_function(query,data);
        if(result){
            let vendor_query = "SELECT  `uuid` as image_id,`image_url` FROM `vendors_shops_image` WHERE id= ?";
            let vendor_id = result.insertId
            let vendor_data = await mysql.query_function(vendor_query,vendor_id);
            if(vendor_data.length>0){
                res.status(200).json({ status: 200, message: 'Image Uploaded',data:vendor_data[0] });
            }else{
                res.status(200).json({ status: 404, message: 'No vendor Found' }); 
            }
        }
        }
        
    }
}
module.exports = vendor_details;