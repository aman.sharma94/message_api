const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const vendors = require('./routes/vendors');
const cors = require('cors');
const app = express();


const cors_options = {
    origin: '*',
    credentials: true
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(helmet.xssFilter());
app.use(helmet.frameguard());
app.use(cors(cors_options));
app.use('/', vendors);

app.use(function(req, res, next) {
    var err = new Error('Not found');
    err.status = 404;
    next(err);
})


//error handler 

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({ status: 400, message: 'Not found' });
})
module.exports = app;