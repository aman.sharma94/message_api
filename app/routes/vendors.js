const express = require('express');
const routes = express.Router();
const vendor_controller = require('../controller/vendor/vendor_controller');
const vendors_details = require('../controller/vendor/vendor_details');
const jwt = require('../middleware/jsonwebtoken');
const multer = require('multer');
const fs = require('fs');
const moment = require('moment');

const storage = multer.diskStorage({
    destination:function(req,file,cb){
            if(!fs.existsSync('./uploads/vendors')){
                fs.mkdirSync('./uploads/vendors');
            }
            cb(null,'./uploads/vendors')
        
    },
    filename:function(req,file,cb){
        cb(null,moment.now()+'-'+file.originalname);
    }
})
const upload_file = multer({
    storage:storage,
    fileFilter:function(req,file,cb){
        if(file.mimetype!='image/png' && file.mimetype!='image/jpeg'&& file.mimetype!='image/jpg'){
            return cb(new Error('File Type Not Allowed'), false);
        }else{
            cb(null,true)
        }
    },
limits:{fileSize:1000000},
}).single('shop_image');

var FileUpload = {
    checkUpload:function(req,res,next){
        upload_file(req,res,function(err){
        if(err){
            res.status(409).json({ status: 409, message: err.message })
        }else{
            next();
        }
    })
    }
}

routes.post('/v1/vendors/login',vendor_controller.signin);
routes.post('/v1/vendors/signup', vendor_controller.sign_up);
routes.post('/v1/vendors/add/shop',jwt.verify_jwt,vendors_details.add_shop);
routes.post('/v1/vendors/shop/image',jwt.verify_jwt,FileUpload.checkUpload,vendors_details.add_image);
module.exports = routes;