const mysql = require('mysql');
const database = require('./databases');
const connection = mysql.createConnection({
    host: database.mysql.host,
    user: database.mysql.user,
    password: database.mysql.password,
    database: database.mysql.database

});
module.exports = connection;