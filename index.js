const app = require('./app/app');
const port = 3001;
const http = require('http');
const server = http.createServer(app);

server.listen(port, function() {
    console.log('Server is starting on ' + port);
})