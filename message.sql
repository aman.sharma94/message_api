-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2020 at 03:54 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `message`
--

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` bigint(255) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `fullName` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `verifyEmail` int(1) NOT NULL DEFAULT '0',
  `mobileNo` varchar(20) NOT NULL,
  `mobileVerify` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `uuid`, `fullName`, `email`, `password`, `verifyEmail`, `mobileNo`, `mobileVerify`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 'a0a2b170-a405-11ea-9311-53df1c4e5bfb', 'Aman Sharma', 'aammaann89@gmail.com', '$2b$10$65UcETuK5oepuzSN29xd/.XveWjOnBIYsncHhOW.a/PpG1bhWQyqi', 0, '9718368408', 0, '2020-06-01 12:44:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendors_shops`
--

CREATE TABLE `vendors_shops` (
  `id` bigint(255) NOT NULL,
  `vendor_id` varchar(255) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `shop_name` varchar(255) NOT NULL,
  `shop_address` varchar(255) NOT NULL,
  `shop_phone` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors_shops`
--

INSERT INTO `vendors_shops` (`id`, `vendor_id`, `uuid`, `shop_name`, `shop_address`, `shop_phone`, `state`, `location`, `pincode`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 'a0a2b170-a405-11ea-9311-53df1c4e5bfb', '91b39710-a4a0-11ea-9739-797623ff1062', 'furniture', '276/26 sector 8', '9718368408', 'haryana', 'Gurugram', '122001', '2020-06-02 07:13:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendors_shops_image`
--

CREATE TABLE `vendors_shops_image` (
  `id` bigint(255) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `vendor_id` varchar(255) NOT NULL,
  `shop_id` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobileNo` (`mobileNo`);

--
-- Indexes for table `vendors_shops`
--
ALTER TABLE `vendors_shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors_shops_image`
--
ALTER TABLE `vendors_shops_image`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendors_shops`
--
ALTER TABLE `vendors_shops`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendors_shops_image`
--
ALTER TABLE `vendors_shops_image`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
